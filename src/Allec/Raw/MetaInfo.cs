﻿using System.Text.Json.Serialization;

namespace Allec.Raw
{
    public class MetaInfo
    {
        [JsonPropertyName("availableCount")]
        public int AvailableCount { get; set; }

        [JsonPropertyName("totalCount")]
        public int TotalCount { get; set; }

        [JsonPropertyName("fallback")]
        public bool Fallback { get; set; }
    }
}
