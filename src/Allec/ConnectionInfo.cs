﻿
using System;

namespace Allec
{
    public class ConnectionInfo
    {
        /// <summary>
        /// Host name of Allegro's test environment
        /// </summary>
        public const string SandboxHost = "allegro.pl.allegrosandbox.pl";

        /// <summary>
        /// Host name of Allegro's production environment
        /// </summary>
        public const string ProdHost = "allegro.pl";

        /// <summary>
        /// Host to be accessed, one of <see cref="SandboxHost"/> or <see cref="ProdHost"/>.
        /// </summary>
        public string Host
        {
            get { return _host; }
            set
            {
                ValidateHost(value);
                _host = value;
            }
        }

        /// <summary>
        /// Client authentication's id
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// Client authentication's secret
        /// </summary>
        public string ClientSecret { get; set; }

        public ConnectionInfo(string host, string clientId, string clientSecret)
        {
            ValidateHost(host);
            _host = host;
            ClientId = clientId;
            ClientSecret = clientSecret;
        }

        // private quaters ------------------------------------------------------------------------

        private string _host;
        private void ValidateHost(string host)
        {
            var h = host.ToLower();
            if (!h.Equals(SandboxHost) && !h.Equals(ProdHost))
            {
                var message = $"Host not valid. It must be one of '{SandboxHost}' or '{ProdHost}'";
                throw new ArgumentException(message);
            }
        }

    }
}
