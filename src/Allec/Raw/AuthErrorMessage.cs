﻿using System.Text.Json.Serialization;

namespace Allec.Raw
{
    public class AuthErrorMessage
    {
        [JsonPropertyName("error")]
        public string Error { get; set; }

        [JsonPropertyName("error_description")]
        public string Description { get; set; }

        public override string ToString()
        {
            return $"Error: {Error}; description: {Description}";
        }
    }
}
