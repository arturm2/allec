﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Allec.Raw
{
    public class CategoryList
    {
        [JsonPropertyName("categories")]
        public IList<Category> Categories { get; set; }
    }
}
