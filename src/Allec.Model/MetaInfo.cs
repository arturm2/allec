﻿namespace Allec.Model
{
    /// <summary>
    /// Search result meta information DTO, details here: https://developer.allegro.pl/documentation/#operation/getListing
    /// </summary>
    public class MetaInfo
    {
        public int AvailableCount { get; set; }

        public int TotalCount { get; set; }

        public bool Fallback { get; set; }

        public override string ToString()
        {
            return $"MetaInfo [available: {AvailableCount}, total: {TotalCount}]";
        }
    }
}
