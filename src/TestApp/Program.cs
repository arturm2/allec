﻿using Allec;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            // configure

            var config = new ConfigurationBuilder()
                .SetBasePath(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location))
                .AddJsonFile("appsettings.json")
                .AddUserSecrets("653cbea6-d396-48b7-a7ca-0aa82f83bc4c")
                .Build();

            var loggerFactory = LoggerFactory.Create(builder =>
            {
                builder
                    .AddConsole()
                    .AddConfiguration(config.GetSection("Logging"));
            });


            // connect

            var connInfo = new ConnectionInfo(
                ConnectionInfo.ProdHost,
                config.GetValue<string>("AllecTestApp:AllegroClientId"),
                config.GetValue<string>("AllecTestApp:AllegroClientSecret"));

            var client = new AllegroClient(connInfo, loggerFactory);


            // retrieve categories ----------------------------------------------------------------

            var cats = client.RetrieveCategories().Result;
            Console.WriteLine($"retrieved {cats.Count} categories");
            foreach (var cat in cats)
            {
                Console.WriteLine(cat);
            }
            Console.WriteLine();


            // do a search ------------------------------------------------------------------------

            var result = client.RetrieveSearchResult("laptop hp", "42540aec-367a-4e5e-b411-17c09b08e41f").Result;

            Console.WriteLine($"total items found: {result.MetaInfo.TotalCount}, page items: {result.MetaInfo.AvailableCount}");
            Console.WriteLine("example of up to 3 items:");

            var sampleItems = result.Items.Take(3);

            foreach (var item in sampleItems)
            {
                Console.WriteLine($"{item.Id}: {item.Name}");
                foreach (var uri in item.ImageUris.Take(3))
                {
                    Console.WriteLine($"\turi: {uri}");
                }
            }


            Console.WriteLine("hit <enter> to close");
            Console.ReadLine();
        }
    }
}
