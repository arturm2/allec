﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Allec
{
    /// <summary>
    /// Interface for HttpClient used by AllegroClient
    /// </summary>
    public interface IHttpClient
    {
        Task<HttpResponseMessage> SendAsync(HttpRequestMessage httpRequestMessage);
    }
}
