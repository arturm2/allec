﻿using System.Text.Json.Serialization;

namespace Allec.Raw
{
    public class SearchResult
    {
        [JsonPropertyName("items")]
        public ItemList Items { get; set; }

        [JsonPropertyName("searchMeta")]
        public MetaInfo Meta { get; set; }
    }
}
