﻿using Allec.Raw;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Web;

namespace Allec
{
    /// <summary>
    /// Simple Allegro rest client
    /// </summary>
    public class AllegroClient
    {
        /// <summary>
        /// Creates an AllegroClient with <see cref="ConnectionInfo"/>, custom loggerFactory and custom httpClient
        /// <see cref="ILoggerFactory"/>
        /// <see cref="IHttpClient"/>
        /// </summary>
        public AllegroClient(ConnectionInfo connectionInfo, ILoggerFactory loggerFactory, IHttpClient httpClient)
        {
            _connectionInfo = connectionInfo;

            if (httpClient == null) // instantiate a default client
            {
                _httpClient = new HttpClient();
            }
            else
            {
                _httpClient = httpClient;
            }

            var _loggerFactory = loggerFactory;
            if (_loggerFactory == null) // instantiate a default logger
            {
                _loggerFactory = LoggerFactory.Create(builder =>
                {
                    builder.AddConsole();
                });
            }
            _logger = _loggerFactory.CreateLogger<AllegroClient>();
        }

        /// <summary>
        /// Creates an AllegroClient with <see cref="ConnectionInfo"/> and a custom loggerFactory
        /// <see cref="ILoggerFactory"/>
        /// </summary>
        public AllegroClient(ConnectionInfo connectionInfo, ILoggerFactory loggerFactory)
            : this(connectionInfo, loggerFactory, null)
        {
            _connectionInfo = connectionInfo;
        }

        /// <summary>
        /// Creates an AllegroClient with <see cref="ConnectionInfo"/>, default loggerFactory and default httpClient
        /// </summary>
        public AllegroClient(ConnectionInfo connectionInfo) :
            this(connectionInfo, null, null)
        {
            this._connectionInfo = connectionInfo;
        }

        /// <summary>
        /// Retrieves authentication token
        /// </summary>
        public async Task<Model.AuthToken> RetrieveTokenAsync()
        {
            var stopwatch = GetStopwatch();

            var url = GetAuthUrl(authPath);
            _logger.LogDebug("Retrieving authToken ({0})", authPath);

            var request = new HttpRequestMessage(HttpMethod.Post, url);
            var byteArray = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{_connectionInfo.ClientId}:{_connectionInfo.ClientSecret}"));
            request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", byteArray);

            string body = await HandleResponseAsync(request);

            var authToken = JsonSerializer.Deserialize<AuthToken>(body);

            stopwatch.Stop();
            _logger.LogInformation("Retrieved auth token ({0:0.00} s)", stopwatch.ElapsedMilliseconds / 1000.0);
            return authToken.GetModel();
        }

        /// <summary>
        /// Retrieves categories of parent category.
        /// If parent is null, top-level categories are retrieved.
        /// </summary>
        public async Task<IList<Model.Category>> RetrieveCategoriesAsync(string parentId = null)
        {
            var stopwatch = GetStopwatch();

            // prepare path
            var path = categoriesPath;
            if (!string.IsNullOrEmpty(parentId))
            {
                path += $"?parent.id={parentId}";
            }

            // request/response
            var request = await PrepareRequestAsync(path);
            _logger.LogDebug("Retrieving categories ({0})", request.RequestUri);
            var body = await HandleResponseAsync(request);

            stopwatch.Stop();
            _logger.LogInformation("Retrieved categories ({0:0.00} s)", stopwatch.ElapsedMilliseconds / 1000.0);

            return JsonSerializer.Deserialize<CategoryList>(body).GetModel();
        }

        /// <summary>
        /// Retrieves search result in a specified category and by a specified phrase
        /// </summary>
        public async Task<Model.SearchResult> RetrieveSearchResultAsync(string phrase, string categoryId = null, int? offset = null, int? limit = null)
        {
            var stopwatch = GetStopwatch();

            // validate params
            if (offset.HasValue ^ limit.HasValue)
            {
                throw new ArgumentException("Parameters offset and limit must be both null xor both empty.");
            }

            // prepare parameters
            var parameters = new Dictionary<string, string> {
                { "searchMode", "REGULAR" },
                { "phrase", HttpUtility.UrlEncode(phrase) }
            };
            if (!string.IsNullOrEmpty(categoryId))
            {
                parameters.Add("category.id", categoryId);
            }
            if (offset.HasValue && limit.HasValue)
            {
                parameters.Add("limit", limit.ToString());
                parameters.Add("offset", offset.ToString());
            }

            var path = QueryHelpers.AddQueryString(searchPath, parameters);

            // request response
            var request = await PrepareRequestAsync(path);
            _logger.LogDebug("Retrieving search results ({0})", request.RequestUri);
            var body = await HandleResponseAsync(request);

            stopwatch.Stop();
            _logger.LogInformation("Retrieved search results ({0:0.00} s)", stopwatch.ElapsedMilliseconds / 1000.0);
            return JsonSerializer.Deserialize<SearchResult>(body).GetModel();
        }


        // private quaters ------------------------------------------------------------------------

        private IHttpClient _httpClient;
        private Model.AuthToken _authToken;
        private readonly ILogger _logger;
        private readonly ConnectionInfo _connectionInfo;

        // rest methods paths
        internal const string authPath = "auth/oauth/token?grant_type=client_credentials";
        internal const string categoriesPath = "sale/categories";
        internal const string searchPath = "offers/listing";
        // http statuses
        private const int httpErrorStatus = 400;
        private readonly int[] httpAuthErrorStatus = new int[] { 401 };

        private string GetAuthUrl(string path)
        {
            return $"https://{_connectionInfo.Host}/{path}";
        }

        private string GetUrl(string path)
        {
            return $"https://api.{_connectionInfo.Host}/{path}";
        }

        private async Task<Model.AuthToken> GetTokenAsync()
        {
            if (_authToken == null)
            {
                // todo: lock here?
                _authToken = await RetrieveTokenAsync();
            }
            return _authToken;
        }

        /// <summary>
        /// Prepares common GET json request
        /// </summary>
        /// <returns>HttpWebRequest</returns>
        private async Task<HttpRequestMessage> PrepareRequestAsync(string path)
        {
            var url = GetUrl(path);
            var authToken = await GetTokenAsync();

            var request = new HttpRequestMessage(HttpMethod.Get, url);
            request.Headers.Add("Authorization", $"Bearer {authToken.AccessToken}");
            request.Headers.Add("Accept", "application/vnd.allegro.public.v1+json");

            return request;
        }

        /// <summary>
        /// Common handling of the response.
        /// Returns string to be converted to json.
        /// </summary>
        private async Task<string> HandleResponseAsync(HttpRequestMessage request)
        {
            string body = null;
            int status = default(int);

            try
            {
                using (var httpResponse = await _httpClient.SendAsync(request))
                {
                    status = (int)httpResponse.StatusCode;
                    body = await httpResponse.Content.ReadAsStringAsync();
                }
            }
            catch (Exception e)
            {
                _logger.LogError("Unexpected error", e);
                throw new ApplicationException("Unexpected error", e);
            }

            if (httpAuthErrorStatus.Contains(status))
            {
                var errorMessage = JsonSerializer.Deserialize<AuthErrorMessage>(body);
                _logger?.LogError(errorMessage.ToString());
                throw new ApplicationException($"Authentication error: {errorMessage}");
            }

            if (status >= httpErrorStatus)
            {
                var errorMessage = JsonSerializer.Deserialize<ErrorList>(body);
                _logger?.LogError(errorMessage.ToString());
                throw new ApplicationException($"API error: {errorMessage.ToUserMessage()}");
            }

            return body;
        }

        /// <summary>
        /// Creates and starts a stopwatch
        /// </summary>
        private Stopwatch GetStopwatch()
        {
            var sw = new Stopwatch();
            sw.Start();
            return sw;
        }
    }
}
