﻿using System.Text.Json.Serialization;

namespace Allec.Raw
{
    public class Category
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("parent")]
        public CategoryParent Parent { get; set; }

        [JsonPropertyName("leaf")]
        public bool Leaf { get; set; }

        // todo: Options missing here, etc.

        public override string ToString()
        {
            return $"Category [id: {Id}, name: {Name}]";
        }
    }
}
