﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Allec
{
    /// <summary>
    /// Default http client of AllegroClient, simple wrapper for .NET's default HttpClient
    /// </summary>
    public class HttpClient : IHttpClient
    {
        private readonly System.Net.Http.HttpClient httpClient = new System.Net.Http.HttpClient();
        public Task<HttpResponseMessage> SendAsync(HttpRequestMessage httpRequestMessage)
        {
            return this.httpClient.SendAsync(httpRequestMessage);
        }
    }
}
