﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Allec.Raw
{
    public class ItemList
    {
        [JsonPropertyName("promoted")]
        public IList<Item> Promoted { get; set; }

        [JsonPropertyName("regular")]
        public IList<Item> Regular { get; set; }
    }
}
