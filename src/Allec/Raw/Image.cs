﻿using System.Text.Json.Serialization;

namespace Allec.Raw
{
    public class Image
    {
        [JsonPropertyName("url")]
        public string Url { get; set; }
    }
}
