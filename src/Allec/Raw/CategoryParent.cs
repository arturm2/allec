﻿using System.Text.Json.Serialization;

namespace Allec.Raw
{
    public class CategoryParent
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }
    }
}
