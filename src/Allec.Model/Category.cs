﻿namespace Allec.Model
{
    /// <summary>
    /// Category DTO, details here: https://developer.allegro.pl/documentation/#operation/getCategoriesUsingGET
    /// </summary>
    public class Category
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ParentCategoryId { get; set; }
        public bool Leaf { get; set; }
        public override string ToString()
        {
            return $"Category [id: {Id}, name: {Name}]";
        }
    }
}
