﻿using System.Text.Json.Serialization;

namespace Allec.Raw
{
    public class ErrorMessage
    {
        [JsonPropertyName("code")]
        public string Code { get; set; }

        [JsonPropertyName("message")]
        public string Message { get; set; }

        [JsonPropertyName("details")]
        public string Details { get; set; }

        [JsonPropertyName("path")]
        public string Path { get; set; }

        [JsonPropertyName("userMessage")]
        public string UserMessage { get; set; }

        /// <summary>
        /// Gets detailed error message (eg. for logging)
        /// </summary>
        public override string ToString()
        {
            return $"error [code: {Code}, message: {Message}, details: {Details}, path: {Path}, userMessage: {UserMessage}]";
        }

        /// <summary>
        /// Gets user-friendly error message
        /// </summary>
        public string ToUserMessage()
        {
            return $"error [code: {Code}, message: {UserMessage}]";
        }
    }
}
