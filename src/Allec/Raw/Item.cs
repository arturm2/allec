﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Allec.Raw
{
    public class Item
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }
        [JsonPropertyName("name")]
        public string Name { get; set; }
        [JsonPropertyName("images")]
        public IList<Image> Images { get; set; }
    }
}
