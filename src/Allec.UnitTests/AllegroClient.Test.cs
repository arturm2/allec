using Moq;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Allec.UnitTests
{
    public class AllegroClientTest : IDisposable
    {

        private readonly ConnectionInfo _connectionInfo;
        private readonly Mock<IHttpClient> _httpClientMock;
        private readonly AllegroClient _allegroClient;

        public AllegroClientTest()
        {
            _connectionInfo = new ConnectionInfo(ConnectionInfo.SandboxHost, "", "");
            _httpClientMock = new Mock<IHttpClient>();
            _allegroClient = new AllegroClient(_connectionInfo, null, _httpClientMock.Object);
        }

        public void Dispose()
        {
        }

        [Fact]
        public void RetrieveToken_OK()
        {
            SetupOkResponse();

            var token = _allegroClient.RetrieveTokenAsync().Result;

            Assert.NotNull(token);
            Assert.Equal("bearer", token.TokenType);
        }

        [Fact]
        public void RetrieveCategories_OK()
        {
            SetupOkResponse();

            var cats = _allegroClient.RetrieveCategoriesAsync().Result;

            Assert.NotNull(cats);
            Assert.Equal(13, cats.Count);
        }

        [Fact]
        public void RetrieveSearchResult_OK()
        {
            SetupOkResponse();

            var result = _allegroClient.RetrieveSearchResultAsync("A", "B").Result;

            Assert.NotNull(result);
            Assert.Equal(6000, result.MetaInfo.AvailableCount);
            Assert.Equal(681939, result.MetaInfo.TotalCount);
            Assert.False(result.MetaInfo.Fallback);
            Assert.Equal(60, result.Items.Count);
        }

        [Fact]
        public void RetrieveSearchResult_Parameters()
        {
            SetupOkResponse();

            // async code throws AggregateException
            var exc = Assert.Throws<AggregateException>(() => _allegroClient.RetrieveSearchResultAsync("A", "B", 3).Result);

            Assert.IsType<ArgumentException>(exc.InnerExceptions[0]);
        }

        [Fact]
        public void RetrieveToken_401()
        {
            SetupFailureResponse(HttpStatusCode.Unauthorized);

            // async code throws AggregateException
            var exc = Assert.Throws<AggregateException>(() => _allegroClient.RetrieveTokenAsync().Result);

            Assert.IsType<ApplicationException>(exc.InnerExceptions[0]);
        }

        [Fact]
        public void RetrieveCategories_404()
        {
            SetupFailureResponse(HttpStatusCode.NotFound);

            // async code throws AggregateException
            var exc = Assert.Throws<AggregateException>(() => _allegroClient.RetrieveTokenAsync().Result);

            Assert.IsType<ApplicationException>(exc.InnerExceptions[0]);
        }


        // private quarters -----------------------------------------------------------------------

        private HttpResponseMessage CreateResponseMessage(HttpStatusCode statusCode, string content)
        {
            byte[] bytes = Encoding.ASCII.GetBytes(content);
            Stream stream = new MemoryStream(bytes);
            var response = new HttpResponseMessage(statusCode);
            response.Content = new StreamContent(stream);
            return response;
        }

        private void SetupOkResponse()
        {
            _httpClientMock
                .Setup<Task<HttpResponseMessage>>(m => m.SendAsync(It.IsAny<HttpRequestMessage>()))
                .Returns((HttpRequestMessage request) =>
                {
                    if (request.RequestUri.AbsoluteUri.EndsWith(AllegroClient.authPath))
                    {
                        var content = File.ReadAllText("./testdata/auth-token.json");
                        return Task.FromResult(CreateResponseMessage(HttpStatusCode.OK, content));
                    }
                    else if (request.RequestUri.AbsoluteUri.Contains(AllegroClient.categoriesPath))
                    {
                        var content = File.ReadAllText("./testdata/categories.json");
                        return Task.FromResult(CreateResponseMessage(HttpStatusCode.OK, content));
                    }
                    if (request.RequestUri.AbsoluteUri.Contains(AllegroClient.searchPath))
                    {
                        var content = File.ReadAllText("./testdata/search-result.json");
                        return Task.FromResult(CreateResponseMessage(HttpStatusCode.OK, content));
                    }
                    return null;
                });
        }

        private void SetupFailureResponse(HttpStatusCode statusCode)
        {
            _httpClientMock
                .Setup<Task<HttpResponseMessage>>(m => m.SendAsync(It.IsAny<HttpRequestMessage>()))
                .Returns((HttpRequestMessage request) =>
                {
                    if (statusCode == HttpStatusCode.Unauthorized)
                    {
                        var content = File.ReadAllText("./testdata/error-401.json");
                        return Task.FromResult(CreateResponseMessage(statusCode, content));
                    }
                    else if (statusCode == HttpStatusCode.NotFound)
                    {
                        var content = File.ReadAllText("./testdata/error-404.json");
                        return Task.FromResult(CreateResponseMessage(statusCode, content));
                    }
                    return null;
                });
        }
    }
}