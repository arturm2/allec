﻿using Allec.Raw;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Allec
{
    /// <summary>
    /// Utilities to convert Raw models to Model models
    /// </summary>
    public static class Converters
    {
        // todo: add unit tests

        public static IList<Model.Category> GetModel(this CategoryList categoryList)
        {
            return categoryList.Categories
                .Select(CategoryConverter)
                .ToList();
        }

        public static Model.SearchResult GetModel(this Raw.SearchResult searchResult)
        {
            return new Model.SearchResult
            {
                Items = searchResult.Items.Regular
                    .Select(ItemConverter)
                    .Concat(searchResult.Items.Promoted
                        .Select(ItemConverter))
                    .ToList(),
                MetaInfo = MetaInfoConverter(searchResult.Meta)
            };
        }

        public static Model.AuthToken GetModel(this Raw.AuthToken authToken)
        {
            return AuthTokenConverter(authToken);
        }

        static Func<Raw.AuthToken, Model.AuthToken> AuthTokenConverter = (t) =>
        {
            return new Model.AuthToken
            {
                AccessToken = t.AccessToken,
                ExpiresIn = t.ExpiresIn,
                Jti = t.Jti,
                RefreshToken = t.RefreshToken,
                Scope = t.Scope,
                TokenType = t.TokenType
            };
        };

        static Func<Raw.Category, Model.Category> CategoryConverter = (c) =>
        {
            return new Model.Category
            {
                Id = c.Id,
                Name = c.Name,
                Leaf = c.Leaf,
                ParentCategoryId = c.Parent?.Id
            };
        };

        static Func<Raw.Image, Uri> ImageConverter = (i) =>
        {
            return new Uri(i.Url);
        };

        static Func<Raw.Item, Model.Item> ItemConverter = (i) =>
        {
            return new Model.Item
            {
                Id = i.Id,
                Name = i.Name,
                ImageUris = i.Images.Select(ImageConverter).ToList()
            };
        };

        static Func<Raw.MetaInfo, Model.MetaInfo> MetaInfoConverter = (m) =>
        {
            return new Model.MetaInfo
            {
                AvailableCount = m.AvailableCount,
                TotalCount = m.TotalCount,
                Fallback = m.Fallback
            };
        };

    }
}
