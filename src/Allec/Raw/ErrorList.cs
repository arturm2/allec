﻿using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;

namespace Allec.Raw
{
    public class ErrorList
    {
        [JsonPropertyName("errors")]
        public IList<ErrorMessage> Errors { get; set; }

        /// <summary>
        /// Gets detailed error message (eg. for logging)
        /// </summary>
        public override string ToString()
        {
            return string.Join(", ", Errors.Select(e => e.ToString()));
        }

        /// <summary>
        /// Gets user-friendly error message
        /// </summary>
        public string ToUserMessage()
        {
            return string.Join("; ", Errors.Select(e => e.ToUserMessage()));
        }
    }
}
