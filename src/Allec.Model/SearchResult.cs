﻿using System.Collections.Generic;

namespace Allec.Model
{
    /// <summary>
    /// Search result DTO, details here: https://developer.allegro.pl/documentation/#operation/getListing
    /// </summary>
    public class SearchResult
    {
        public IList<Item> Items { get; set; }
        public MetaInfo MetaInfo { get; set; }
    }
}
