# AllegroClient
A client library for Allego REST API

.NET Core 3.1

Supports limited number of features:
 * Category retrieval
 * Search results

# Install
Download the sources and build (no nuget published).

# Usage
See TestApp for example.
Get started with
 1. Register at https://developer.allegro.pl/, create client id and secret
 2. Use the following code

```
	var connInfo = new ConnectionInfo(host: "allegro.pl.allegrosandbox.pl", clientId: "...", clientSecret: "...");
	var client = new AllegroClient(connInfo);
	var cats = client.RetrieveCategories().Result;
```
