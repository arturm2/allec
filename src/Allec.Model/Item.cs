﻿using System;
using System.Collections.Generic;

namespace Allec.Model
{
    /// <summary>
    /// Item (or offer) DTO, details here: https://developer.allegro.pl/documentation/#operation/getListing
    /// </summary>
    public class Item
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public IList<Uri> ImageUris { get; set; }

        public override string ToString()
        {
            return $"Item [id: {Id}, name: {Name}]";
        }
    }
}
