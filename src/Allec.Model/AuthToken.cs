﻿namespace Allec.Model
{
    /// <summary>
    /// AuthToken DTO, details here: https://developer.allegro.pl/auth/#CredentialsFlow
    /// </summary>
    public class AuthToken
    {
        public string AccessToken { get; set; }

        public string TokenType { get; set; }

        public string RefreshToken { get; set; }

        public int ExpiresIn { get; set; }

        public string Scope { get; set; }

        public string Jti { get; set; }
    }
}
